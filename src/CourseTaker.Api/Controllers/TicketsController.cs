﻿using CourseTaker.Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseTaker.Api.Controllers
{
    [Route("courses/{courseId}/tickets")]
    [Authorize]
    public class TicketsController : ApiControllerBase
    {
        private readonly ITicketService _ticketService;

        public TicketsController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpGet("{ticketId}")]
        public async Task<IActionResult> Get(Guid courseId, Guid ticketId)
        {
            var ticket = await _ticketService.GetAsync(UserId, courseId, ticketId);
            if (ticket == null)
            {
                return NotFound();
            }
            return Json(ticket);
        }

        [HttpPost("purchase/{amount}")]
        public async Task<IActionResult> Post(Guid courseId, int amount)
        {
            await _ticketService.PurchaseAsync(UserId, courseId, amount);

            return NoContent();
        }

        [HttpDelete("cancel/{amount}")]
        public async Task<IActionResult> Delete(Guid courseId, int amount)
        {
            await _ticketService.CancelAsync(UserId, courseId, amount);

            return NoContent();
        }
    }
}
