﻿using CourseTaker.Infrastructure.Commands.Courses;
using CourseTaker.Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseTaker.Api.Controllers
{
    [Route("[controller]")]
    public class CoursesController : ApiControllerBase
    {
        private readonly ICourseService _courseService;

        public CoursesController(ICourseService courseService)
        {
            _courseService = courseService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string name)
        {
            var courses = await _courseService.BrowseAsync(name);

            return Json(courses);
        }

        [HttpGet("{courseId}")]
        public async Task<IActionResult> Get(Guid courseId)
        {
            var course = await _courseService.GetAsync(courseId);
            if (course == null)
            {
                return NotFound();
            }
            return Json(course);
        }

        [HttpPost]
        [Authorize(Policy = "HasAdminRole")]
        public async Task<IActionResult> Post([FromBody]CreateCourse command)
        {
            command.CourseId = Guid.NewGuid();
            await _courseService.CreateAsync(command.CourseId, command.Name, 
                command.Description, command.StartDate, command.EndDate);
            await _courseService.AddTicketsAsync(command.CourseId, command.Tickets, command.Price);

            return Created($"/courses/{command.CourseId}", null);
        }

        [HttpPut("{courseId}")]
        [Authorize(Policy = "HasAdminRole")]
        public async Task<IActionResult> Put(Guid courseId, [FromBody]UpdateCourse command)
        {
            await _courseService.UpdateAsync(courseId, command.Name, command.Description);

            return NoContent();
        }

        [HttpDelete("{courseId}")]
        [Authorize(Policy = "HasAdminRole")]
        public async Task<IActionResult> Delete(Guid courseId)
        {
            await _courseService.DeleteAsync(courseId);

            return NoContent();
        }
    }
}
