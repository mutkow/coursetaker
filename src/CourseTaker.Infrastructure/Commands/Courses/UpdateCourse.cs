﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseTaker.Infrastructure.Commands.Courses
{
    public class UpdateCourse
    {
        public Guid CourseId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
