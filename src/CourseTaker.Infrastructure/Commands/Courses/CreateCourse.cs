﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseTaker.Infrastructure.Commands.Courses
{
    public class CreateCourse
    {
        public Guid CourseId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal Price { get; set; }

        public int Tickets { get; set; }
    }
}
