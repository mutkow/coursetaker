﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseTaker.Infrastructure.DTO
{
    public class TicketDetailsDto : TicketDto
    {
        public Guid CourseId { get; set; }

        public string CourseName { get; set; }
    }
}
