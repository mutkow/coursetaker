﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseTaker.Infrastructure.DTO
{
    public class CourseDetailsDto : CourseDto
    {
        public IEnumerable<TicketDto> Tickets { get; set; }
    }
}
