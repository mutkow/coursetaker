﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CourseTaker.Infrastructure.Services
{
    public class DataInitializer : IDataInitializer
    {
        private readonly IUserService _userService;
        private readonly ICourseService _courseService;

        public DataInitializer(IUserService userService, ICourseService courseService)
        {
            _userService = userService;
            _courseService = courseService;
        }

        public async Task SeedAsync()
        {
            var tasks = new List<Task>();
            tasks.Add(_userService.RegisterAsync(Guid.NewGuid(), "user", "User1", "user1@wp.pl", "secret"));
            tasks.Add(_userService.RegisterAsync(Guid.NewGuid(), "admin", "Admin1", "admin1@wp.pl", "secret"));
            for(var i=1; i<10; i++)
            {
                var courseId = Guid.NewGuid();
                var name = $"Course {i}";
                var description = $"{name} decription";
                var startDate = DateTime.UtcNow.AddHours(2);
                var endDate = DateTime.UtcNow.AddHours(3);
                tasks.Add(_courseService.CreateAsync(courseId, name, description, startDate, endDate));
                tasks.Add(_courseService.AddTicketsAsync(courseId, 3, 30));
            }
            await Task.WhenAll(tasks);
        }
    }
}
