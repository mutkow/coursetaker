﻿using CourseTaker.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseTaker.Infrastructure.Services
{
    public interface IJwtHandler
    {
        JwtDto CreateToken(Guid userId, string role);
    }
}
