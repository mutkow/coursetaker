﻿using CourseTaker.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CourseTaker.Infrastructure.Services
{
    public interface ITicketService
    {
        Task<TicketDto> GetAsync(Guid userId, Guid courseId, Guid ticketId);

        Task PurchaseAsync(Guid userId, Guid courseId, int amount);

        Task CancelAsync(Guid userId, Guid courseId, int amount);

        Task<IEnumerable<TicketDetailsDto>> GetForUserAsync(Guid userId);
    }
}
