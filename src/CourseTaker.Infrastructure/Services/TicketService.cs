﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CourseTaker.Infrastructure.DTO;
using AutoMapper;
using CourseTaker.Core.Repositories;
using CourseTaker.Infrastructure.Extensions;
using System.Linq;

namespace CourseTaker.Infrastructure.Services
{
    public class TicketService : ITicketService
    {
        private IUserRepository _userRepository;
        private ICourseRepository _courseRepository;
        private IMapper _mapper;

        public TicketService(IUserRepository userRepository, ICourseRepository courseRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _courseRepository = courseRepository;
            _mapper = mapper;
        }

        public async Task<TicketDto> GetAsync(Guid userId, Guid courseId, Guid ticketId)
        {
            var user = await _userRepository.GetOrFailUserAsync(userId);
            var ticket = await _courseRepository.GetOrFailTicketAsync(courseId, ticketId);

            return _mapper.Map<TicketDto>(ticket);
        }

        public async Task PurchaseAsync(Guid userId, Guid courseId, int amount)
        {
            var user = await _userRepository.GetOrFailUserAsync(userId);
            var course = await _courseRepository.GetOrFailAsync(courseId);
            course.PurchaseTickets(user, amount);
            await _courseRepository.UpdateAsync(course);
        }

        public async Task CancelAsync(Guid userId, Guid courseId, int amount)
        {
            var user = await _userRepository.GetOrFailUserAsync(userId);
            var course = await _courseRepository.GetOrFailAsync(courseId);
            course.CancelPurchasedTickets(user, amount);
            await _courseRepository.UpdateAsync(course);
        }


        public async Task<IEnumerable<TicketDetailsDto>> GetForUserAsync(Guid userId)
        {
            var user = await _userRepository.GetOrFailUserAsync(userId);
            var courses = await _courseRepository.BrowseAsync();

            var allTickets = new List<TicketDetailsDto>();
            foreach (var course in courses)
            {
                var tickets = _mapper.Map<IEnumerable<TicketDetailsDto>>(course.GetTicketsPurchasedByUser(user))
                    .ToList();
                tickets.ForEach(x =>
                {
                    x.CourseId = course.Id;
                    x.CourseName = course.Name;
                });
                allTickets.AddRange(tickets);
            }

            return allTickets;

        }
    }
}
