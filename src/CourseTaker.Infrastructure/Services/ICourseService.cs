﻿using CourseTaker.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CourseTaker.Infrastructure.Services
{
    public interface ICourseService
    {
        Task<CourseDetailsDto> GetAsync(Guid id);

        Task<CourseDetailsDto> GetAsync(string name);

        Task<IEnumerable<CourseDto>> BrowseAsync(string name = "");

        Task CreateAsync(Guid id, string name, string description, DateTime startDate, DateTime endDate);

        Task UpdateAsync(Guid id, string name, string description);

        Task DeleteAsync(Guid id);

        Task AddTicketsAsync(Guid eventId, int amount,  decimal price);
    }
}
