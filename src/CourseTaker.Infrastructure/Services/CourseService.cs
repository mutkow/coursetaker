﻿using AutoMapper;
using CourseTaker.Core.Domain;
using CourseTaker.Core.Repositories;
using CourseTaker.Infrastructure.DTO;
using CourseTaker.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CourseTaker.Infrastructure.Services
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository _courseRepository;
        private readonly IMapper _mapper;

        public CourseService(ICourseRepository courseRepository, IMapper mapper)
        {
            _courseRepository = courseRepository;
            _mapper = mapper;
        }

        //get_async_using_id_should_invoke_get_assync_on_course_repository()
        public async Task<CourseDetailsDto> GetAsync(Guid id)
        {
            var course = await _courseRepository.GetAsync(id);

            return _mapper.Map<CourseDetailsDto>(course);
        }

        // public async Task get_async_using_name_should_invoke_get_assync_on_course_repository()
        public async Task<CourseDetailsDto> GetAsync(string name)
        {
            var course = await _courseRepository.GetAsync(name);

            return _mapper.Map<CourseDetailsDto>(course);
        }

        // browse_async_should_invoke_all_courses_and_get_browse_repository()
        public async Task<IEnumerable<CourseDto>> BrowseAsync(string name = "")
        {
            var courses = await _courseRepository.BrowseAsync(name);

            return _mapper.Map<IEnumerable<CourseDto>>(courses);
        }

        //create_course_async_should_invoke_add_assync_on_course_repository()
        public async Task CreateAsync(Guid id, string name, string description, DateTime startDate, DateTime endDate)
        {
            var course = await _courseRepository.GetAsync(name);
            if (course != null)
            {
                throw new Exception($"Course named '{name}' already exists.");
            }
            course = new Course(id, name, description, startDate, endDate);
            await _courseRepository.AddAsync(course);
        }

        //update_async_course_should_invoke_update_on_course_repository()
        public async Task UpdateAsync(Guid id, string name, string description)
        {
            var course = await _courseRepository.GetAsync(name);
            if (course != null)
            {
                throw new Exception($"Course id '{name}' already exists.");
            }
            course = await _courseRepository.GetOrFailAsync(id);
            course.SetName(name);
            course.SetDescription(description);
            await _courseRepository.UpdateAsync(course);
        }

        // adding_tickets_async_should_add_tickets_to_list()
        public async Task AddTicketsAsync(Guid courseId, int amount, decimal price)
        {
            var course = await _courseRepository.GetOrFailAsync(courseId);
            course.AddTickets(amount, price);
            await _courseRepository.UpdateAsync(course);
        }

        //delete_course_async_should_invoke_delete_assync_on_course_repository_and_delete_from_list()
        public async Task DeleteAsync(Guid id)
        {
            var course = await _courseRepository.GetOrFailAsync(id);
            await _courseRepository.DeleteAsync(course);
        }
    }
}
