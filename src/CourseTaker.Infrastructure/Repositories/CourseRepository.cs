﻿using CourseTaker.Core.Domain;
using CourseTaker.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseTaker.Infrastructure.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        private static readonly ISet<Course> _courses = new HashSet<Course>();
       

        // when_getting_course_id_it_should_return_course()
        public async Task<Course> GetAsync(Guid id)
        {
            return await Task.FromResult(_courses.SingleOrDefault(x => x.Id == id));
        }

        //when_getting_course_name_it_should_return_course()
        public async Task<Course> GetAsync(string name)
        {
            return await Task.FromResult(_courses.SingleOrDefault(x => x.Name.ToLowerInvariant() == name.ToLowerInvariant()));
        }

        public async Task<IEnumerable<Course>> BrowseAsync(string name = "")
        {
            var courses = _courses.AsEnumerable();
            if (!string.IsNullOrWhiteSpace(name))
            {
                courses = courses.Where(x => x.Name.ToLowerInvariant()
                    .Contains(name.ToLowerInvariant()));
            }
            return await Task.FromResult(courses);
        }
        //when_adding_new_courser_id_should_be_added_to_list()
        public async Task AddAsync(Course course)
        {
            _courses.Add(course);
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(Course course)
        {
            await Task.CompletedTask;
        }

        //when_remove_course_it_should_be_removed_from_list()
        public async Task DeleteAsync(Course course)
        {
            _courses.Remove(course);
            await Task.CompletedTask;
        }
    }
}
