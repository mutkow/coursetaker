﻿using CourseTaker.Core.Domain;
using CourseTaker.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseTaker.Infrastructure.Extensions
{
    public static class RepositoryExtensions
    {
        public static async Task<Course> GetOrFailAsync(this ICourseRepository repository, Guid id)
        {
            var course = await repository.GetAsync(id);
            if (course == null)
            {
                throw new Exception($"Course id '{id}' does not exists.");
            }

            return course;
        }


        public static async Task<User> GetOrFailUserAsync(this IUserRepository repository, Guid id)
        {
            var user = await repository.GetAsync(id);
            if (user == null)
            {
                throw new Exception($"Event id '{id}' does not exists.");
            }

            return user;
        }

        public static async Task<Ticket> GetOrFailTicketAsync(this ICourseRepository repository, Guid eventId, Guid ticketId)
        {
            var @event = await repository.GetOrFailAsync(eventId);
            var ticket = @event.Tickets.SingleOrDefault(x => x.Id == ticketId);
            if (ticket == null)
            {
                throw new Exception($"Ticket with id '{ticketId}' was not found for event");
            }

            return ticket;
        }
    }
}
