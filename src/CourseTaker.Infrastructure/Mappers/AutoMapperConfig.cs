﻿using AutoMapper;
using CourseTaker.Core.Domain;
using CourseTaker.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseTaker.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
           => new MapperConfiguration(cfg =>
           {
               cfg.CreateMap<Course, CourseDto>()
                   .ForMember(x => x.TicketsCount, m => m.MapFrom(p => p.Tickets.Count()))
                   .ForMember(x => x.AvailableTicketsCount, m => m.MapFrom(p => p.AvailableTickets.Count()))
                   .ForMember(x => x.PurchasedTicketsCount, m => m.MapFrom(p => p.PurchasedTickets.Count()));
               cfg.CreateMap<Course, CourseDetailsDto>();
               cfg.CreateMap<Ticket, TicketDto>();
               cfg.CreateMap<Ticket, TicketDetailsDto>();
               cfg.CreateMap<User, AccountDto>();
           })
           .CreateMapper();
    }
}
