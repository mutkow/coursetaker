﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseTaker.Infrastructure.Settings
{
    public class AppSettings
    {
        public bool SeedData { get; set; }
    }
}
