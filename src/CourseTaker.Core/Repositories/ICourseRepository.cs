﻿using CourseTaker.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CourseTaker.Core.Repositories
{
    public interface ICourseRepository
    {
        Task<Course> GetAsync(Guid id);

        Task<Course> GetAsync(string name);

        Task<IEnumerable<Course>> BrowseAsync(string name = "");

        Task AddAsync(Course course);

        Task UpdateAsync(Course course);

        Task DeleteAsync(Course course);
    }
}
