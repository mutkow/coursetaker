﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseTaker.Core.Domain
{
    public class Course : Entity
    {
        private ISet<Ticket> _tickets = new HashSet<Ticket>();

        public string Name { get; protected set; }

        public string Description { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime StartDate { get; protected set; }

        public DateTime EndDate { get; protected set; }

        public IEnumerable<Ticket> Tickets => _tickets;

        public IEnumerable<Ticket> PurchasedTickets => Tickets.Where(x => x.Purchased);

        public IEnumerable<Ticket> AvailableTickets => Tickets.Except(PurchasedTickets);

        protected Course()
        {
        }

        public Course(Guid id, string name, string description, DateTime startDate, DateTime endDate)
        {
            Id = id;
            SetName(name);
            SetDescription(description);
            SetDates(startDate, endDate);
            CreatedAt = DateTime.UtcNow;
        }

        public void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Name can not be null");
            }
            Name = name;
        }

        public void SetDescription(string description)
        {
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new Exception("Description can not be null");
            }
            Description = description;
        }

        public void SetDates(DateTime startDate, DateTime endDate)
        {
            if (startDate >= endDate)
            {
                throw new Exception($"Course with id: {Id} must have a end date greater than start date.");
            }
            StartDate = startDate;
            EndDate = endDate;
        }

        public void AddTickets(int amount, decimal price)
        {
            for (var i = 0; i < amount; i++)
            {
                _tickets.Add(new Ticket(this, price));
            }
        }

        public void PurchaseTickets(User user, int amount)
        {
            if (AvailableTickets.Count() < amount)
            {
                throw new Exception("Not enough tickets to purchase");
            }
            var tickets = AvailableTickets.Take(amount);
            foreach (var ticket in tickets)
            {
                ticket.Purchase(user);
            }
        }

        public void CancelPurchasedTickets(User user, int amount)
        {
            var tickets = GetTicketsPurchasedByUser(user);
            if (tickets.Count() < amount)
            {
                throw new Exception("Not enough purchased tickets to be cancelled");
            }
            foreach (var ticket in tickets.Take(amount))
            {
                ticket.Cancel();
            }
        }

        public IEnumerable<Ticket> GetTicketsPurchasedByUser(User user)
          => PurchasedTickets.Where(x => x.UserId == user.Id);

    }
}
