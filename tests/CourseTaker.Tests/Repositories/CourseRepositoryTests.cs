﻿using CourseTaker.Core.Domain;
using CourseTaker.Core.Repositories;
using CourseTaker.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CourseTaker.Tests.Repositories
{
    public class CourseRepositoryTests
    {

        //s
        [Fact]
        public async Task when_adding_new_courser_id_should_be_added_to_list()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course1", "Course1Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            ICourseRepository repository = new CourseRepository();

            //Act
            await repository.AddAsync(course);

            //Assert
            var existingCourse = await repository.GetAsync(course.Id);
            Assert.Equal(course, existingCourse);
        }

        [Fact]
        public async Task when_getting_course_name_it_should_return_course()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            ICourseRepository repository = new CourseRepository();

            //Act
            await repository.AddAsync(course);

            //Assert
            var existingCourse = await repository.GetAsync(course.Name);
            Assert.Equal(course, existingCourse);
        }

        [Fact]
        public async Task when_remove_course_it_should_be_removed_from_list()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            ICourseRepository repository = new CourseRepository();

            //Act
            await repository.DeleteAsync(course);

            //Assert
            var existingCourse = await repository.GetAsync(course.Id);
            Assert.Equal(null, existingCourse);
        }

        [Fact]
        public async Task when_getting_course_id_it_should_return_course()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            ICourseRepository repository = new CourseRepository();

            //Act
            await repository.AddAsync(course);

            //Assert
            var existingCourse = await repository.GetAsync(course.Id); ;
            Assert.Equal(course, existingCourse);
        }

    }
}
