﻿using CourseTaker.Core.Domain;
using CourseTaker.Core.Repositories;
using CourseTaker.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CourseTaker.Tests.Repositories
{
    public class UserRepositoryTests
    {
        //s
        [Fact]
        public async Task when_adding_new_user_it_should_be_added_to_list()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "user", "Grzegorz", "szrt@poczta.onet.pl", "secret");
            IUserRepository repository = new UserRepository();

            //Act
            await repository.AddAsync(user);

            //Assert
            var existingUser = await repository.GetAsync(user.Id);
            Assert.Equal(user, existingUser);
        }

        [Fact]
        public async Task when_remove_user_it_should_be_removed_from_list()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "user", "Grzegorz", "szrt@poczta.onet.pl", "secret");
            IUserRepository repository = new UserRepository();

            //Act
            await repository.DeleteAsync(user);

            //Assert
            var existingUser = await repository.GetAsync(user.Id);
            Assert.Equal(null, existingUser);
        }

        [Fact]
        public async Task when_getting_user_email_it_should_return_user()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "user", "Grzegorz", "szrt@poczta.onet.pl", "secret");
            IUserRepository repository = new UserRepository();
            var email = "szrt@poczta.onet.pl";

            //Act
            await repository.AddAsync(user);

            //Assert
            var existingUser = await repository.GetAsync(email);
            Assert.Equal(user, existingUser);
        }

        [Fact]
        public async Task when_getting_user_id_it_should_return_user()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "user", "Grzegorz", "szrt@poczta.onet.pl", "secret");
            IUserRepository repository = new UserRepository();
            var userId = user.Id;

            //Act
            await repository.AddAsync(user);

            //Assert
            var existingUser = await repository.GetAsync(userId);
            Assert.Equal(user, existingUser);
        }
    }
}
