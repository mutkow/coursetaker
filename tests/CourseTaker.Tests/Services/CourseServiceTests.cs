﻿using AutoMapper;
using CourseTaker.Core.Domain;
using CourseTaker.Core.Repositories;
using CourseTaker.Infrastructure.DTO;
using CourseTaker.Infrastructure.Services;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CourseTaker.Tests.Services
{
    public class CourseServiceTests
    {
        //s
        [Fact]
        public async Task create_course_async_should_invoke_add_assync_on_course_repository()
        {
            //Arrange
            var courseRepositoryMock = new Mock<ICourseRepository>();
            var mapper = new Mock<IMapper>();
            var courseService = new CourseService(courseRepositoryMock.Object, mapper.Object);

            //Act
            await courseService.CreateAsync(Guid.NewGuid(), "Course1Test", "Course1Desc", DateTime.UtcNow, DateTime.UtcNow.AddHours(2));

            //Assert
            courseRepositoryMock.Verify(x => x.AddAsync(It.IsAny<Course>()), Times.Once);
        }

        //s
        [Fact]
        public async Task get_async_using_id_should_invoke_get_assync_on_course_repository()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2Test", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            var courseRepositoryMock = new Mock<ICourseRepository>();
            var mapper = new Mock<IMapper>();
            var courseService = new CourseService(courseRepositoryMock.Object, mapper.Object);
            var courseDetailsDto = new CourseDetailsDto
            {
                Id = course.Id,
                Name = course.Name,
                Description = course.Description,
                StartDate = course.StartDate,
                TicketsCount = course.Tickets.Count(),
            };

            courseRepositoryMock.Setup(x => x.GetAsync(course.Id)).ReturnsAsync(course);
            mapper.Setup(x => x.Map<CourseDetailsDto>(course)).Returns(courseDetailsDto);

            //Act
            var existingCourseDto = await courseService.GetAsync(course.Id);

            //Assert
            courseRepositoryMock.Verify(x => x.GetAsync(course.Id), Times.Once);
            existingCourseDto.Should().NotBeNull();
            existingCourseDto.Description.ShouldBeEquivalentTo(courseDetailsDto.Description);
        }

        [Fact]
        public async Task get_async_using_name_should_invoke_get_assync_on_course_repository()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2Test", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            var courseRepositoryMock = new Mock<ICourseRepository>();
            var mapper = new Mock<IMapper>();
            var courseService = new CourseService(courseRepositoryMock.Object, mapper.Object);
            var courseDetailsDto = new CourseDetailsDto
            {
                Id = course.Id,
                Name = course.Name,
                Description = course.Description,
                StartDate = course.StartDate,
                TicketsCount = course.Tickets.Count(),
            };

            courseRepositoryMock.Setup(x => x.GetAsync(course.Name)).ReturnsAsync(course);
            mapper.Setup(x => x.Map<CourseDetailsDto>(course)).Returns(courseDetailsDto);

            //Act
            var existingCourseDto = await courseService.GetAsync(course.Name);

            //Assert
            courseRepositoryMock.Verify(x => x.GetAsync(course.Name), Times.Once);
            existingCourseDto.Should().NotBeNull();
            existingCourseDto.Description.ShouldBeEquivalentTo(courseDetailsDto.Description);
        }

        [Fact]
        public async Task browse_async_should_invoke_all_courses_and_get_browse_repository()
        {
            //Arrange 
            var courses = new List<Course>()
            {
                new Course(Guid.NewGuid(), "Course2Test", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4)),
                new Course(Guid.NewGuid(), "Course2Test", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4)),
            };
            var coursesDto = new List<CourseDto>()
            {
                new CourseDto(),
                new CourseDto(),
            };
            var courseRepositoryMock = new Mock<ICourseRepository>();
            var mapper = new Mock<IMapper>();
            var courseService = new CourseService(courseRepositoryMock.Object, mapper.Object);
            courseRepositoryMock.Setup(x => x.BrowseAsync("")).ReturnsAsync(courses);
            mapper.Setup(x => x.Map<IEnumerable<CourseDto>>(courses)).Returns(coursesDto);

            //Act
            var existingCoursesDto = await courseService.BrowseAsync("");

            //Assert
            courseRepositoryMock.Verify(x => x.BrowseAsync(""), Times.Once);
            existingCoursesDto.Should().NotBeNull();
            existingCoursesDto.Count().ShouldBeEquivalentTo(courses.Count());
        }

        [Fact]
        public async Task update_async_course_should_invoke_update_on_course_repository()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2Test", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            var courseRepositoryMock = new Mock<ICourseRepository>();
            var mapper = new Mock<IMapper>();
            var courseService = new CourseService(courseRepositoryMock.Object, mapper.Object);
            courseRepositoryMock.Setup(x => x.GetAsync(course.Id)).ReturnsAsync(course);

            //Act
            await courseService.UpdateAsync(course.Id, "gucio", "gucio");

            //Assert
            courseRepositoryMock.Verify(x => x.UpdateAsync(course), Times.Once);
            course.Should().NotBeNull();
            course.Description.ShouldBeEquivalentTo("gucio");
            course.Name.ShouldBeEquivalentTo("gucio");
        }

        [Fact]
        public async Task adding_tickets_async_should_add_tickets_to_list()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2Test", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            var courseRepositoryMock = new Mock<ICourseRepository>();
            var mapper = new Mock<IMapper>();
            var courseService = new CourseService(courseRepositoryMock.Object, mapper.Object);
            var ticketAmount = 3;
            var ticketPrice = 12;
            courseRepositoryMock.Setup(x => x.GetAsync(course.Id)).ReturnsAsync(course);

            //Act
            await courseService.AddTicketsAsync(course.Id, ticketAmount, ticketPrice);

            //Assert
            courseRepositoryMock.Verify(x => x.UpdateAsync(course), Times.Once);
            course.Should().NotBeNull();
            course.Tickets.Count().ShouldBeEquivalentTo(ticketAmount);
        }

        [Fact]
        public async Task delete_course_async_should_invoke_delete_assync_on_course_repository_and_delete_from_list()
        {
            //Arrange
            var course = new Course(Guid.NewGuid(), "Course2Test", "Course2Desc", DateTime.UtcNow.AddHours(2), DateTime.UtcNow.AddHours(4));
            var courseRepositoryMock = new Mock<ICourseRepository>();
            var mapper = new Mock<IMapper>();
            var courseService = new CourseService(courseRepositoryMock.Object, mapper.Object);
            courseRepositoryMock.Setup(x => x.GetAsync(course.Id)).ReturnsAsync(course);

            //Act
            await courseService.DeleteAsync(course.Id);

            //Assert
            courseRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<Course>()), Times.Once);
        }
    }
}
