﻿using AutoMapper;
using CourseTaker.Core.Domain;
using CourseTaker.Core.Repositories;
using CourseTaker.Infrastructure.DTO;
using CourseTaker.Infrastructure.Services;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CourseTaker.Tests.Services
{
    public class UserServiceTests
    {
        //s
        [Fact]
        public async Task register_async_should_invoke_add_assync_on_user_repository()
        {
            //Arrange
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapper = new Mock<IMapper>();
            var jwtHandler = new Mock<IJwtHandler>();
            var userService = new UserService(userRepositoryMock.Object, jwtHandler.Object, mapper.Object);

            //Act
            await userService.RegisterAsync(Guid.NewGuid(), "test@test.pl", "test", "secret");

            //Assert
            userRepositoryMock.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Once);
        }

        //s
        [Fact]
        public async Task when_invoking_get_account_async_should_return_accountdto()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "user", "Grzegorz", "szrt@poczta.onet.pl", "secret");
            var accountDto = new AccountDto
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Role = user.Role
            };
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapper = new Mock<IMapper>();
            var jwtHandler = new Mock<IJwtHandler>();
            var userService = new UserService(userRepositoryMock.Object, jwtHandler.Object, mapper.Object);
            userRepositoryMock.Setup(x => x.GetAsync(user.Id)).ReturnsAsync(user);
            mapper.Setup(x => x.Map<AccountDto>(user)).Returns(accountDto);
            //Act

            var existingUserDto = await userService.GetAccountAsync(user.Id);

            //Assert
            userRepositoryMock.Verify(x => x.GetAsync(user.Id), Times.Once);
            userRepositoryMock.Should().NotBeNull();
            existingUserDto.Email.ShouldBeEquivalentTo(accountDto.Email);
        }
    }
}
